import peewee as pw

import db


class Promo(db.BaseModel):
    id = pw.BigAutoField()
    name = pw.CharField()
    description = pw.CharField(null=True)


class Prize(db.BaseModel):
    id = pw.BigAutoField()
    description = pw.CharField()
    promo = pw.ForeignKeyField(Promo,
        backref='prizes', on_delete='CASCADE'
    )


class Participant(db.BaseModel):
    id = pw.BigAutoField()
    name = pw.CharField()
    promo = pw.ForeignKeyField(Promo,
        backref='participants', on_delete='CASCADE'
    )


class Result(db.BaseModel):
    id = pw.BigAutoField()
    promo = pw.ForeignKeyField(Promo,
        backref='results', on_delete='CASCADE'
    )
    prize = pw.ForeignKeyField(Prize,
        backref='+', on_delete='CASCADE'
    )
    winner = pw.ForeignKeyField(Participant,
        backref='+', on_delete='CASCADE'
    )


def init():
    db.db.connect()
    db.db.create_tables((Promo, Prize, Participant, Result))
    db.db.close()

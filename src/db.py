import os
import peewee as pw
import playhouse.db_url as db_url

db = db_url.connect(os.environ['DB_URL'])


class BaseModel(pw.Model):
    class Meta:
        database = db


class DatabaseMiddleware:
	def process_resource(self, req, resp, resource, params):
		db.connect()

	def process_response(self, req, resp, resource, req_succeeded):
		if not db.is_closed():
			db.close()

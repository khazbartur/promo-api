from playhouse.shortcuts import model_to_dict
import falcon
import peewee
import random

from models import Promo, Participant, Prize, Result


class PromoResource:
    def on_post(self, req, resp):
        name = req.media.get('name')
        description = req.media.get('description')
        if not name:
            raise falcon.HTTPBadRequest

        promo = Promo.create(name=name, description=description)
        resp.media = promo.id

    def on_get(self, req, resp):
        promos = Promo.select()
        resp.media = [model_to_dict(promo) for promo in promos]

    def on_get_by_id(self, req, resp, promo_id):
        promo = Promo.get_or_none(id=promo_id)
        if promo is None:
            raise falcon.HTTPNotFound
        resp.media = model_to_dict(promo, backrefs=True, exclude=[Promo.results])

    def on_put_by_id(self, req, resp, promo_id):
        name = req.media.get('name')
        description = req.media.get('description')
        if not name:
            raise falcon.HTTPBadRequest

        promo = Promo.get_or_none(id=promo_id)
        if promo is None:
            raise falcon.HTTPNotFound

        promo.name = name
        promo.description = description
        promo.save()

    def on_delete_by_id(self, req, resp, promo_id):
        promo = Promo.get_or_none(id=promo_id)
        if promo is None:
            raise falcon.HTTPNotFound

        promo.delete_instance()


class ParticipantResource:
    def on_post(self, req, resp, promo_id):
        name = req.media.get('name')
        if not name:
            raise falcon.HTTPBadRequest

        promo = Promo.get_or_none(id=promo_id)
        if promo is None:
            raise falcon.HTTPNotFound
        
        participant = Participant.create(name=name, promo=promo)
        resp.media = participant.id

    def on_delete_by_id(self, req, resp, promo_id, participant_id):
        participant = Participant.get_or_none(id=participant_id, promo=promo_id)
        if participant is None:
            raise falcon.HTTPNotFound

        participant.delete_instance()


class PrizeResource:
    def on_post(self, req, resp, promo_id):
        description = req.media.get('description')
        if not description:
            raise falcon.HTTPBadRequest

        promo = Promo.get_or_none(id=promo_id)
        if promo is None:
            raise falcon.HTTPNotFound
        
        prize = Prize.create(description=description, promo=promo)
        resp.media = prize.id

    def on_delete_by_id(self, req, resp, promo_id, prize_id):
        prize = Prize.get_or_none(id=prize_id, promo=promo_id)
        if prize is None:
            raise falcon.HTTPNotFound

        prize.delete_instance()


class RaffleResource:
    def on_post(self, req, resp, promo_id):
        promo = Promo.get_or_none(id=promo_id)
        if promo is None:
            raise falcon.HTTPNotFound

        if len(promo.participants) != len(promo.prizes):
            return falcon.HTTPConflict

        winners = list(promo.participants)
        random.shuffle(winners)

        Result.bulk_create(
            Result(promo=promo, prize=prize.id, winner=winner.id)
            for (winner, prize) in zip(promo.prizes, winners)
        )
        results = Result.select().where(Result.promo == promo)

        resp.media = [
            model_to_dict(result, exclude=[Result.id, Result.promo])
            for result in results
        ]

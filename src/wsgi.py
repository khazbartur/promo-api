import falcon
import os
import waitress

import db
import res
import models


def make_app():
    models.init()
    application = falcon.App(middleware=[
        db.DatabaseMiddleware()
    ])

    res_promo = res.PromoResource()
    res_participant = res.ParticipantResource()
    res_prize = res.PrizeResource()
    res_raffle = res.RaffleResource()

    application.add_route('/promo',
        res_promo
    )
    application.add_route('/promo/{promo_id:int}',
        res_promo, suffix='by_id'
    )
    application.add_route('/promo/{promo_id:int}/participant',
        res_participant,
    )
    application.add_route('/promo/{promo_id:int}/participant/{participant_id:int}',
        res_participant, suffix='by_id'
    )
    application.add_route('/promo/{promo_id:int}/prize',
        res_prize,
    )
    application.add_route('/promo/{promo_id:int}/prize/{prize_id:int}',
        res_prize, suffix='by_id'
    )
    application.add_route('/promo/{promo_id:int}/raffle',
        res_raffle,
    )

    return application


def serve_app(application):
    port = os.environ['PORT']
    print(f'Serving app on port {port}...')
    waitress.serve(application, port=port)


application = make_app()

if __name__ == '__main__':
    serve_app(application)
